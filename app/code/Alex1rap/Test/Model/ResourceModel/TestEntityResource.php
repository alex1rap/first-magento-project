<?php

namespace Alex1rap\Test\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TestEntityResource extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('test_entity', 'entity_id');
    }
}
