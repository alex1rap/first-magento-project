<?php

namespace Alex1rap\Test\Model\ResourceModel\TestEntity;

use Alex1rap\Test\Model\ResourceModel\TestEntityResource;
use Alex1rap\Test\Model\TestEntity;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(TestEntity::class, TestEntityResource::class);
    }
}
