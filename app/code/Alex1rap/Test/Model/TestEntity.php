<?php

namespace Alex1rap\Test\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class TestEntity extends AbstractModel
{
    public function __construct(
        Context $context,
        Registry $registry,
        ResourceModel\TestEntityResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_init('Alex1rap\Test\Model\ResourceModel\TestEntityResource');
    }
}
