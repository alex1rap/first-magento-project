<?php

namespace Alex1rap\Test\Setup\Patch\Data;

use Alex1rap\Test\Model\ResourceModel\TestEntityResource;
use Alex1rap\Test\Model\TestEntityFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddTestEntity implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var TestEntityFactory
     */
    private $testEntityFactory;
    /**
     * @var TestEntityResource
     */
    private $testEntityResource;

    public function __construct(
        TestEntityFactory $TestEntityFactory,
        TestEntityResource $testEntityResource,
        ModuleDataSetupInterface $moduleDataSetup
    )
    {
        $this->testEntityFactory = $TestEntityFactory;
        $this->testEntityResource = $testEntityResource;
        $this->moduleDataSetup=$moduleDataSetup;
    }
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $testEntityDTO=$this->testEntityFactory->create();
        $testEntityDTO->setName('Some Test Entity')->setType('SomeEntityType')->setDescription('Some description');
        $this->testEntityResource->save($testEntityDTO);
        $this->moduleDataSetup->endSetup();
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
