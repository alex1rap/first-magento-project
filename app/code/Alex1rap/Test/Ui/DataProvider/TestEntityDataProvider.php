<?php

namespace Alex1rap\Test\Ui\DataProvider;

use Alex1rap\Test\Model\ResourceModel\TestEntity\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class TestEntityDataProvider extends AbstractDataProvider
{
    public function __construct(
        CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create();
        $this->collection
            ->addFieldToSelect('*');
    }
}
