<?php

namespace Alex1rap\Test\Controller\Adminhtml\Test;

use Alex1rap\Test\Model\TestEntity;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;

class Delete extends Action
{

    const ADMIN_RESOURCE = 'Alex1rap_Test::test_delete';

    /**
     * @var TestEntity
     */
    protected $testEntity;

    public function __construct(Context $context, TestEntity $testEntity)
    {
        parent::__construct($context);
        $this->testEntity = $testEntity;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->testEntity;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Record deleted successfully.'));
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
            }
        }
        $this->messageManager->addError(__('Record does not exist.'));
        return $resultRedirect->setPath('*/*/');
    }
}
