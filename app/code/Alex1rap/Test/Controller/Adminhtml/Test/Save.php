<?php

namespace Alex1rap\Test\Controller\Adminhtml\Test;

use Alex1rap\Test\Model\TestEntity;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use RuntimeException;

class Save extends Action
{
    /**
     * @var TestEntity
     */
    protected $testEntity;
    /**
     * @var Session
     */
    protected $adminSession;

    /**
     * @param Context $context
     * @param TestEntity $testEntity
     * @param Session $adminSession
     */
    public function __construct(
        Context $context,
        TestEntity $testEntity,
        Session $adminSession
    )
    {
        parent::__construct($context);
        $this->testEntity = $testEntity;
        $this->adminSession = $adminSession;
    }

    /**
     * Save blog record action
     *
     * @return Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $entity_id = $this->getRequest()->getParam('general[entity_id]');
            if ($entity_id) {
                $this->testEntity->load($entity_id);
            }
            $this->testEntity->setData($data['general']);
            try {
                $this->testEntity->save();
                $this->messageManager->addSuccess(__('The data has been saved.'));
                $this->adminSession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    if ($this->getRequest()->getParam('back') == 'add') {
                        return $resultRedirect->setPath('*/*/add');
                    } else {
                        return $resultRedirect->setPath('*/*/edit', [
                            'entity_id' => $this->testEntity->getEntityId(),
                            '_current' => true
                        ]);
                    }
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [
                'entity_id' => $this->getRequest()->getParam('entity_id')
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
