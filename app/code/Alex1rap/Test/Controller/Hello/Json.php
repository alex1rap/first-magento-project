<?php

namespace Alex1rap\Test\Controller\Hello;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Json
 * @package Alex1rap\Test\Controller\Hello
 */
class Json extends Action
{
    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $jsonResult = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $jsonResult->setData([
            'message' => 'Hello World!'
        ]);
        return $jsonResult;
    }
}
