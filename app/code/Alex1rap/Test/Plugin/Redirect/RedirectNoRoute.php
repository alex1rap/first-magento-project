<?php

namespace Alex1rap\Test\Plugin\Redirect;

use Closure;
use Magento\Cms\Controller\Noroute\Index;
use Magento\Framework\Controller\Result\RedirectFactory;

class RedirectNoRoute
{
    private $redirectFactory;

    public function __construct(RedirectFactory $redirectFactory)
    {
        $this->redirectFactory = $redirectFactory;
    }

    public function aroundExecute(Index $index, Closure $proceed)
    {
        return $this->redirectFactory->create()->setPath('/');
    }
}
