<?php

namespace Alex1rap\Test\Plugin\Model;

use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ChangeProductName
 * @package alex1rap\Plugin\Model
 */
class ChangeProductName
{
    /**
     * @var Session
     */
    private $_session;

    /**
     * ProductNameChanger constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->_session = $session;
    }

    /**
     * @param Product $product
     * @param string $name
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterGetName(Product $product, string $name): string
    {
        return $this->_session->getCustomerGroupId() === Group::NOT_LOGGED_IN_ID ? $name : "Product: {$name}";
    }
}
