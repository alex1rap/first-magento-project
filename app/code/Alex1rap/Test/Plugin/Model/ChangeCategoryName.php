<?php

namespace Alex1rap\Test\Plugin\Model;

use Magento\Catalog\Model\Category;

/**
 * Class ChangeCategoryName
 * @package alex1rap\Plugin\Model
 */
class ChangeCategoryName
{

    /**
     * @param Category $category
     * @param string $name
     * @return string
     */
    public function afterGetName(Category $category, string $name): string
    {
        return "Hello World {$name}";
    }

}
